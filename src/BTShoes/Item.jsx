import React from 'react'

const Item = (props) => {
    
    const {item,HandleProductDetail}=props; 
    return (
    

      <div key={item.id} className=" col-4 mt-3">
                <div className="card">
                    <img src={item.image} alt="..." />
                    <div className="card-body">
                        <p className='font-weight-bold'>{item.name}</p>
                        <p>{item.price}</p>
                        <button className='btn btn-outline-success ' data-toggle="modal" data-target="#exampleModal" onClick={()=>HandleProductDetail(item)}>Xem Them</button>
                    
                    </div>

                </div>
            </div>

  )
}

export default Item
